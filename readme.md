# Webentwicklung mit Docker

Dieses Projekt stellt mit Hilfe von Docker eine Entwicklungsumgebung für Webseiten bereit. Zu dieser Umgebung gehören ein Apache-Webserver, PHP 7, mySQL und phpMyAdmin. Darüber hinaus sind folgende Komponenten installiert: mod_rewrite, intl, composer, zip, unzip, git, imagick, gd, poppler.

Das Projekt und die Anleitung sind auf Windows 10 als Betriebssystem ausgerichtet, beides kann aber für andere Betriebssysteme adaptiert werden.

# Bedeutung der Ordner und Dateien

- **html** ist das Wurzelverzeichnis des Webservers, d.h. hier wird die Webanwendung später abgelegt. Vorläufig ist darin eine Datei index.php enthalten, die im Browser grundlegende Daten über den Server ausgibt.
- **php** enthält ein Dockerfile, in dem das Image für den Webserver definiert ist. In der Datei php.ini können bei Bedarf weitere Einstellungen vorgenommen werden, die bei der Einrichtung  in den Webserver übernommen werden.
- **tools** enthält Powershell-Scripte zur Arbeit mit der Entwicklungsumgebung. Zu jedem Powershell-Script gibt es eine Batch-Datei, die unter Windows das Powershell-Script startet (Doppelklick).
- **dump** enthält Scripte zum Sichern der Datenbank. Dieser Ordner wird auch zum Einspielen von Datenbanken in den Server verwendet.
- **docker-compose.yml** definiert den Stack der Entwicklungsumgebung (=Container, Netzwerke, Verzeichnisse)
- **.gitignore** sorgt dafür, dass nur relevante Dateien in der Versionsverwaltung bzw. auf Gitlab landen
- **readme.md** enthält den Text, den Sie gerade lesen

# Voraussetzungen
Für Docker müssen falls möglich und noch nicht erfolgt Virtualisierungsoptionen im BIOS aktiviert werden. Starten Sie Ihren Computer neu, gehen Sie in das BIOS (je nach Computer unterschiedlich, in der Regel über die Tasten Entf oder F2). Suchen Sie dort nach Einträgen mit dem Stichwort Virtualisierung und aktivieren Sie die Optionen. Sie können diesen Schritt aber auch später nachholen, falls etwas nicht funktioniert.

Auf dem Computer muss Docker inklusive Docker Compose installiert werden. Zum Einsatz von Docker unter Windows gibt es drei Szenarien:
- Unter Windows 10 und auf aktueller Hardware kann Docker in Verbindung mit Hyper-V verwendet werden. Darauf ist die **Docker Community Edition for Windows** abgestimmt. Das Installationsprogramm kann nach einer Registrierung kostenlos im Docker Store (store.docker.com) heruntergeladen werden. Ohne Anmeldung ist das Herunterladen über die Seite https://download.docker.com bzw. folgenden Direktlink möglich: https://download.docker.com/win/stable/17513/Docker%20for%20Windows%20Installer.exe. Auch nach der Installation werden Sie möglicherweise darum gebeten, sich mit einem Docker-Konto anzumelden. Sie können diesen Schritt aber (bislang) einfach überspringen bzw. das entsprechende Fenster einfach schließen. Docker nutzt unter Windows standardmäßig die Virtualisierungstechnik Hyper-V und bietet nach der Installation an, Hyper-V zu aktivieren. Danach ist ein Neustart des Systems nötig, der eine Weile dauern kann. 
    
- Für andere Windows-Versionen oder falls Sie Hyper-V nicht verwenden wollen, installieren Sie stattdessen **Docker Toolbox**, siehe https://docs.docker.com/toolbox/overview/ bzw. folgenden Direktlink https://download.docker.com/win/stable/DockerToolbox.exe.  Diese Version funktioniert ohne die Virtualisierungstechnik Hyper-V, beachten Sie im Folgenden die Hinweise zur Kombination von Docker mit VirtualBox.
    
- Sie können statt Docker Toolbox auch die aktuelle **Docker Community Edition for Windows ohne Hyper-V** verwenden. Hyper-V verhindert andere Virtualisierungstechniken wie Virtual Box, das beispielsweise für Vagrant verwendet wird.  Falls Sie auf solche anderen Virtualisierungstechniken angewiesen sind oder auf einem System arbeiten, das kein Hyper-V unterstützt, dann können Sie Docker in einer virtuellen Maschine laufen lassen. Installieren Sie dazu bitte **Virtual Box**, falls es noch nicht auf dem Computer vorhanden ist. Beachten Sie im Folgenden die Hinweise zur Verwendung von Docker in Kombination mit Virtual Box.

    Sie sollten Hyper-V in diesem Fall während der Installation von Docker nicht aktivieren bzw. wieder deaktivieren.  Zum manuellen Aktivieren oder Deaktivieren der Funktion können Sie in der Systemsteuerung (oder einfach die Windows-Taste drücken) nach "Windows-Features aktivieren oder deaktivieren" suchen. 

    Außerdem wird Docker beim Systemstart darauf hinweisen, dass es ohne Hyper-V nicht laufen kann. Sie können diese Meldung ignorieren und den automatischen Start von Docker abschalten (im Taskmanager unter "Autostart", ereichbar über Windows-Taste + X).

Bei der Installation von Docker können Sie wählen, ob Sie mit Windows-Containern arbeiten wollen. Bleiben Sie bei **Linux Containern**, darauf sind das vorliegende Projekt und die meisten Container abgestimmt. Während der Installation müssen Sie sich einmal von Windows abmelden, alle Anwendungen werden dabei geschlossen. Speichern Sie deshalb vorher unbedingt alle Dateien. 

Empfehlenswert ist außerdem die Installation der Versionsverwaltung **Git**.  Git kann von der Projektseite https://git-scm.com/ heruntergeladen werden. Für die folgende Anleitung wird Git ausschließlich auf der Kommandozeile bedient. Zur einfacheren Bedienung können Sie bei anderen Projekten aber auch den mitgelieferten Git-Client verwenden, der eine grafische Oberfläche bereit stellt. Alternativ können Sie auch den kostenlosen GitHub Desktop Client verwenden. Dieser ist besonders einfach zu bedienen und anders als es der Name vermuten lässt nicht auf Projekte bei GitHub beschränkt: https://desktop.github.com/


# Einrichtung

## 1. Kommandozeile im Arbeitsverzeichnis

Wählen Sie auf Ihrem Computer ein **Arbeitsverzeichnis** aus oder erstellen Sie ein Verzeichnis, in dem die Entwicklungsumgebung eingerichtet werden soll. Sie können das Verzeichnis zum Beispiel "webdock" nennen.

Die Einrichtung der Entwicklungsumgebung findet hauptsächlich auf der Kommandozeile statt. Unter Windows kann dafür die eingebaute **PowerShell** verwendet werden. Öffnen Sie im Arbeitsverzeichnis die Kommandozeile. Eine einfache Möglichkeit, um die PowerShell in einem bestimmten Verzeichnis zu starten: den Explorer öffnen, zum gewünschten Verzeichnis gehen und oben in die Adressleiste "powershell" eingeben und mit Enter bestätigen. Sie können die Powershell auch aus dem Startmenü heraus aufrufen. Drücken Sie die Windows-Taste, geben Sie "powershell" ein und klicken Sie den Eintrag "Windows PowerShell" an. Mitunter muss die Kommandozeile im Administratormodus geöffnet werden - das erreichen Sie hier mit einem Rechtsklick und der Option "Als Administrator" ausführen. 

Um das Verzeichnis zu wechseln, verwenden Sie den Befehl `cd` gefolgt vom Zielverzeichnis. Sie können auch relative Pfade angeben, also zum Beispiel Unterverzeichnisse. Es reicht häufig aus, die ersten Buchstaben einzugeben, der Rest kann mit der Tabulatortaste ergänzt werden. Ein Liste der Dateien und Unterverzeichnisse erhalten Sie mit dem Befehl `ls` (unter Linux günstiger: `ls -l`). Zwei Punkte kennzeichnen das übergeordnete Verzeichnis (`cd ..`). 

In der PowerShell können Sie Text über die Zwischenablage kopieren und einfügen (Strg+C und Strg-V). Das funktioniert auch ohne Tasten allein mit der Maus - vor allem, wenn Sie später mit SSH arbeiten. Mit Rechtsklick kopieren Sie markierten Text. Ist kein Text markiert, wird mit Rechtklick Text eingefügt. 


## 2. Dateien herunterladen

Laden Sie nun die Dateien des Projekts herunter und legen diese im Arbeitsverzeichnis ab. Auf der Kommandozeile können die Dateien direkt über git heruntergeladen werden, dieser Vorgang nennt sich klonen:

`git clone https://gitlab.com/strohne/webdock.git .`

Der Punkt am Ende ist wichtig. Er gibt das aktuelle Verzeichnis als Zielverzeichnis an, andernfalls würde ein Unterverzeichnis "webdock" angelegt werden.

## 3. Eine Virtuelle Maschine einrichten
Wenn Sie Docker in Kombination mit VirtualBox verwenden, dann müssen Sie zunächst eine virtuelle Maschine erstellen und diese mit der Kommandozeile verbinden. In Kombination mit Hyper-V können Sie diesen Schritt überspringen.

Mit dem folgenden Befehl in der PowerShell erstellen Sie eine Maschine mit dem Namen "default". Der Vorteil dieses Namens ist, dass dann bei den folgenden docker-machine-Befehlen kein Name angegeben werden muss:

- `docker-machine create default`

Anschließend und nach jedem Neustart des Computers starten Sie von der PowerShell die Maschine:
 
- `docker-machine start`

Finden Sie nun die IP-Adresse der Machine heraus (z.B. 192.168.99.100):

- `docker-machine ip`

Unter dieser Adresse sind die unten beschriebenen Dienste erreichbar. Beachten Sie bitte: immer wenn im folgenden als Adresse "localhost" angegeben ist, müssen Sie statt dessen diese IP-Adresse verwenden. Die IP-Adresse kann sich nach einem Neustart der virtuellen Maschine ändern, bleibt in der Regel aber lange stabil.

Um die aktuell geöffnete Kommandozeile mit dem Docker-Daemon zu verbinden, geben Sie folgende Befehle ein. Dies ist **immer nötig, wenn Sie eine neue PowerShell** öffnen.

- `docker-machine env`
- `docker-machine env | Invoke-Expression`

Ohne diese beiden Befehle geben die Docker-Befehler später eine Fehlermeldung aus ("error during connect").

Zur Erleichterung der Arbeit mit der virtuellen Maschine liegen im Unterverzeichnis "tools/virtualbox" fertige PowerShell-Scripte, die mit einem Doppelklick auf die dazugehörige Batch-Datei gestartet werden können. - siehe unten den Abschnitt 3.Tools.

Mitunter müssen Sie das Arbeitsverzeichnis Ihres Projektes noch einmal extra für Virtual Box freigeben, so dass es dann in den Docker Container weitergereicht werden kann. Standardmäßig sorgt docker-machine dafür, dass Verzeichnisse unterhalb des Windows-Benutzerverzeichnisses zugänglich sind. Falls Ihr Projektverzeichnis aber zum Beispiel auf einer anderen Festplatte liegt, richten Sie über folgende Befehle in der Powershell eine Freigabe ein:

- `docker-machine stop`
- `& "C:\Program Files\Oracle\VirtualBox\vboxmanage.exe" sharedfolder add default --name "e/projekt" --hostpath "e:\projekt" --automount`
- `docker-machine start`

Den Pfad `C:\Program Files\Oracle\VirtualBox\vboxmanage.exe` müssen Sie ggf. anpassen, falls Virtual Box in einem anderen Verzeichnis installiert wurde. Auch die Pfade `e/projekt` und `e:\projekt` passen Sie bitte an Ihre Gegebenheiten an, dabei handelt es sich um den Pfad zum unten eingerichteten Arbeitsverzeichnis. Beachten Sie bitte: die Laufwerksbuchstaben müssen kleingeschrieben werden.

Bevor Sie den Computer später ausschalten, sollten Sie die virtuelle Maschine abschalten:
- `docker-machine stop`


Für weitere Informationen zum Betrieb von Docker mit einer virtuellen Maschine siehe auch https://docs.docker.com/machine/get-started/



## 4. Container starten

Die wichtigste Komponente von Docker sind **Container**. Container stellen einzelne **Dienste** zur Verfügung, beispielsweise einen Webserver oder einen Datenbankserver. Diese Container bauen auf Betriebssystemen wie Ubuntu auf und fügen Funktionen hinzu. Ein solcher Container wird dann als **Image** abgespeichert. Das Image kann weitergegeben werden und es lassen sich vom Image ein oder auch mehrere identische Container starten. Ein Image kann auch dazu verwendet werden, um wiederum andere Images davon abzuleiten und um weitere Funktionen zu ergänzen. Die Ergänzungen werden in Dateien mit dem Namen Dockerfile (ohne Endung) festgelegt. Ein Beispiel für ein auf diese Weise ergänztes Image finden Sie im Unterordner "php".

Eine vollständige Entwicklungsumgebung benötigt in der Regel mehrere Dienste, die über unterschiedliche Container bereit gestellt werden. Ein Container enthält zum Beispiel den Webserver und ein anderer den Datenbankserver. Damit man nicht jeden Container einzeln starten und konfigurieren muss, kann man das Zusammenspiel mit dem Werkzeug **Docker Compose** "orchestrieren".

Das Projekt enthält dazu im Hauptverzeichnis eine Datei docker-compose.yml, die Sie mit einem Texteditor anschauen können. Darin wird festgelegt, welche Container aus welchen Images gestartet werden sollen: einen Webserver mit PHP, einen Datenbankserver mit mySQL und einen weiteren Webserver mit phpMyAdmin. Für den Webserver mit PHP ist das Image im Dockerfile definiert. Die Grundlage besteht aus offiziellen Images vom Docker Hub. Die anderen Container werden ohne Anpassungen direkt aus dem Docker Hub heruntergeladen. 

Die gesamte dort definierte Entwicklungsumgebung kann über die Kommandozeile gestartet werden:

`docker-compose up -d`

Die benötigten Images werden automatisch heruntergeladen bzw. auf Grundlage von Dockerfiles erstellt. Anschließend werden die Container gestartet, mit dem Dateisystem verbunden und miteinander vernetzt. Die Option `-d` sorgt dafür, dass die Kommandozeile nach dem Starten wieder für weitere Befehle zur Verfügung steht (detached mode). Das kann beim ersten Mal eine Weile dauern, die Zeit reicht für einen Tee oder Kaffee. Schon beim zweiten Mal sollten die Container aber blitzschnell starten - das ist ein Vorteil gegenüber der vollständigen Virtualisierung, wie sie etwas mit Vagrant möglich wäre.

Im Zuge der Einrichtung wird ggf. nachgefragt, ob Sie den Zugriff auf das Internet oder auf das Dateisystem zulassen wollen. Bestätigen Sie diese Nachfragen.

Tipp: Sollte das Starten der Container nicht klappen, dann hilft mitunter ein Neustart von Docker. Suchen Sie das Docker-Symbol in der Taskleiste, klicken Sie es mit der rechten Maustaste an und wählen Sie "Restart". Sollte kein Symbol in der Taskleiste zu finden sein (auch nicht durch Erweitern der Taskleiste über den Pfeil), dann ist entweder Docker nicht gestartet oder Sie arbeiten mit docker-machine in Kombination mit VirtualBox. Starten Sie im ersten Fall Docker, etwa indem Sie die Windowstaste drücken, "docker" eingeben und die Anwendung mit der Enter-Taste oder mit der Maus starten. Im zweiten Fall verwenden Sie auf der Kommmandozeile die Befehle `docker-machine stop` und `docker-machine start`.

# In der Entwicklungsumgebung arbeiten

## 1. Den Webserver testen

Nach dem Starten der Container können Sie den __Webserver__ testen. Öffnen Sie Ihren Browser (z.B. Firefox) und rufen Sie dort die Adresse "localhost" auf. Hier sollte eine Begrüßungsseite angezeigt werden. Der Inhalt dieser Seite wird durch das PHP-Script "html/index.php" erzeugt. Bearbeiten Sie die Datei ruhig einmal (einfach etwas hinein schreiben) und laden Sie die Seite im Webbrowser neu (Taste F5 drücken). Ist es nicht faszinierend, wie man seinen Computer mit nur einem Befehl auf der Kommandozeile in einen funktionierenden Webserver verwandeln kann? In dem Verzeichnis mit der index.php oder noch besser in weiteren Unterverzeichnissen können Sie Ihre PHP- oder HTML-Dateien ablegen und über den Browser aufrufen.

Unter der Adresse localhost:8080 sollte sich die Anwendung __phpMyAdmin__ melden. Die Nummer nach dem Doppelpunkt gibt den sogenannten Port an. Der Standardport für Webseiten hat die Nummer 80 und muss normalerweise nicht angegeben werden. Damit sich der Webserver für die eigenen Projekte und phpMyAdmin nicht in die Quere kommen, wird die Anwendung  hier über einen eigenen Container mit einem eigenen Webserver auf dem Port 8080 bereitgestellt. Mit dem Benutzernamen "root" und dem Passwort "root" können Sie sich einloggen und so die MySQL-Datenbanken verwalten. Beim Starten der Container wurde eine leere Datenbank mit dem Namen "devel" angelegt, die hier sichtbar sein sollte. 

Die Daten der __MySQL-Datenbanken__ werden in einem speziellen Verzeichnis (in einem "volume") abgelegt, das von Docker verwaltet wird. Dieses Verzeichnis wird nach dem erstmaligen Starten der Container angelegt. Hier überdauern die Datenbanken selbst wenn die Container wieder gestoppt werden. Zur Datensicherung sollten Sie jedoch regelmäßig einen Dump der Datenbanken anlegen (siehe das Kapitel unten).

Die Datenbanken sind von PHP-Scripten über folgende Zugangsdaten erreichbar (siehe auch die index.php):

- Host: mysql
- Benutzername: root
- Passwort: root

Auch von außerhalb der Container können Sie zum Beispiel mit Anwendungen wie HeidiSQL auf die Datenbanken zugreifen. Dafür müssen Sie den Host anders angeben:

- Host: localhost (oder die IP-Adresse 127.0.0.1)
- Benutzername: root
- Passwort: root

## 2. Docker-Kommandos

Für die Arbeit mit der Entwicklungsumgebung muss man verschiedene Ebenen unterscheiden:

### 2.1 Die Maschine
Auf einer Kommandozeile wie der PowerShell arbeiten Sie zunächst in dem System, auf dem Docker läuft. Hier können Sie Verzeichnisse wechseln und Befehle zur Steuerung der tieferen Ebenen aufrufen. 

Wenn Sie allerdings unter Windows 10 ohne Hyper-V arbeiten, dann wird Docker nicht auf Ihrem System ausgeführt, sondern innerhalb einer virtuellen Maschine. Diese Maschine können Sie teilweise über die Oberfläche von Virtual Box bedienen, besser aber über docker-machine-Befehle auf der Kommandozeile. Geben sie `docker-machine` ein, um ein Liste möglicher Befehle zu sehen. Mit `docker-machine ls` sehen Sie eine Liste eingerichter virtueller Maschinen. Mit `docker-machine start` und `docker-machine stop` können Sie eine Maschine anhalten und starten. Nützlich ist auch der Befehl`docker-machine ip`, durch den Sie die IP-Adresse der Maschine erhalten. Über diese Adresse ist der Webserver im Browser erreichbar. Wenn die Maschine einen anderen Namen als "default" hat, müssen Sie hinter den drei letzten Befehlen noch den Namen der Maschine angeben.

### 2.2 Der gesamte Stack
Mit Docker Compose-Befehlen wird der gesamte Stack verwaltet. Die Umgebung wird über den Befehl `docker-compose up -d` gestartet und über `docker-compose stop` beendet. Die Option -d sorgt dafür, dass die Kommandozeile gleich wieder zur Verfügung steht (=detached). Andernfalls sehen Sie fortlaufend die Ausgabe des letzten Containers. Mit einem doppelten Strg+C kommen Sie in diesem Fall zur Kommandozeile zurück, beenden damit aber auch die Container. Falls ein Container einmal nicht wie gewünscht funktioniert, starten Sie Docker (bzw. die virtuelle Maschine) neu und stoppen und starten Sie alle Container. 

Um Änderungen an einem Image vorzunehmen, kann man die Dockerfile-Datei bearbeiten. Anschließend wird das Image mit `docker-compose build` neu erstellt. Im php-Ordner finden Sie eine solche Datei für den Webserver. Um Einfluss auf die PHP-Einstellungen zu nehmen, können Sie dort auch die php.ini-Datei bearbeiten. Anschließend müssen diese Änderungen mit dem build-Befehl in das Image übertragen werden.

### 2.3 Einzelne Komponenten von Docker
Mit Docker-Befehlen werden in der Regel einzelne Container innerhalb des Stacks angesprochen. Wenn Sie den Befehl `docker` ohne irgendwelche Ergänzungen eingeben, erhalten Sie eine Übersicht über mögliche Befehle. Mit `docker ps` erhalten Sie beispielsweise eine Übersicht über alle laufenden Container. Mit der Option -a können Sie auch beendete Container sehen: `docker ps -a`. Für einzelne Komponenten von Docker gibt es wiederum Unterbefehle, insbesondere für Images, Container, Volumes und Netzwerke. Diese Unterbefehle werden  angezeigt, wenn Sie den Docker-Befehl mit einem der Management-Befehle kombinieren, zum Beispiel `docker container`.  So erhalten Sie mit `docker container ls` eine Liste laufender Container oder mit `docker image ls` eine Liste vorhandener Images.

### 2.4 Das System im Container
Man kann innerhalb eines Containers Linux-Befehle ausführen, indem man eine interaktive Kommandozeile startet: `docker exec -it webdock_php /bin/bash`. Hiermit wird innerhalb des Containers für den Webserver (webdock_php) die Kommandozeile (/bin/bash) gestartet (exec). Die Option -it bewirkt, dass die Kommandozeile nicht sofort wieder geschlossen wird. Erst nach Eingabe von `exit` landen Sie wieder in der Powershell. Innerhalb des Containers können beispielsweise Programme mit dem Paketmanager apt-get nachinstalliert werden. Allerdings sind diese Änderungen nach dem Beenden des Containers verloren, wenn sie nicht in das Dockerfile übertragen werden. Nützlich sind Befehle zur Überprüfung von Dateien. Mit `ls -a` erhalten Sie eine Übersicht über die Dateien im aktuellen Verzeichnis. Mit dem  cd-Befehl können Sie das Verzeichnis wechseln, zum Beispiel mit `cd \var\www\html`. Mit dem cat-Befehl können Sie den Inhalt von Dateien ansehen, zum Beispiel`cat index.php`

### 2.5 Einzelne Services innerhalb eines Containers
Innerhalb eines Containers bringen einzelne Dienste wiederum neue Kommandozeilen mit. Die MySQL-Konsole für die Interaktion mit der Datenbank können Sie innerhalb eines Containers beispielsweise über `mysql -hmysql -uroot -proot` aufrufen. Dabei gibt die Option -h den Host an, -u den Benutzernamen und -p das Passwort. Anschließend können Sie SQL-Befehle eingeben, zum Beispiel `SHOW DATABASES;` um eine Liste aller Datenbanken einzusehen. Die MySQL-Konsole verlassen Sie über den Befehl `exit`.

## 3. Tools
Im Unterverzeichnis "tools" finden Sie eine Auswahl an PowerShell-Scripten (Endung .ps1) zur Arbeit mit Docker. Da man PowerShell-Scripte standardmäßig nicht mit Doppelklick ausführen kann, gibt es zu jedem der Scripte noch eine Batch-Datei (Endung .bat) - ein Doppelklick führt das dazugehörige Script aus.

Die Scripte im Unterordner "hyperv" sind für Docker in Kombination mit Hyper-V gedacht. Falls Sie Docker in Kombination mit VirtualBox verwenden, helfen die Scripte im Unterordner "virtualbox":

- Nach jedem __Neustart des  Computers__ müssen Sie die Entwicklungsumgebung  aktivieren: docker-compose-up.bat startet alle Container und ggf. die virtuelle Maschine. Anschließend gibt das Script die IP-Adresse aus und startet den Browser. 
- Vor dem __Herunterfahren des Computers__ sollten Sie alles wieder beenden: docker-compose-stop.bat stoppt alle Container und ggf. die virtuelle Maschine. 
- Normalerweise muss die Entwicklungsumgebung nach der ersten Einrichtung nicht mehr verändert werden. Falls Sie zur __Administration__ doch einmal in den Container hinein müssen, geht das über SSH: docker-webdock-ssh.bat öffnet eine SSH in dem Container, der den Webserver und PHP enthält.
- Für die __Administation__ benötigen Sie möglicherweise auch eine Kommandozeile, zum Beispiel zum Einspielen von Datenbanken: powershell.bat öffnet eine PowerShell im Projektverzeichnis.



## 4. Datenbanken anlegen

In der Entwicklungsumgebung ist bereits phpMyAdmin installiert, womit Sie die Datenbanken verwalten können. Rufen Sie dazu die Adresse http://localhost:8080 im Browser auf und loggen Sie sich mit Benutzername "root" und Passwort "root" ein. Sie können auch eine Desktop-Anwendung wie HeidiSQL zur Verwaltung verwenden, hier wären als Host "localhost", als Port der Standard-Port von MySQL "3306" und als Benutzername sowie Passwort wiederum "root" anzugeben.

Für umfangreiche Datenbanken oder zur Automatisierung verwendet man dagegen besser die Kommandozeile. Hierfür ist das Unterverzeichnis "dump" vorbereitet, das innerhalb des Webserver-Containers unter "/var/dump" erreichbar ist. 

Öffnen Sie die PowerShell im Unterverzeichnis dump. Sie können von hier mit der Kommandozeile Befehle direkt in den Docker-Container übergeben. Dabei sollten Sie besonders aufmerksam sein, denn es können Daten unwiederbringlich gelöscht werden (wenn sie nicht noch woanders abgelegt sind). 

- Der folgende Befehl __löscht eine vorhandene Datenbank__ mit dem Namen "gso": 
    `docker exec -i -w "/var/dump" webdock_php mysql -hmysql -uroot -proot -e "DROP DATABASE IF EXISTS gso"`

- Mit dem folgenden Befehl legen Sie anschließend eine __neue Datenbank__ an:
    `docker exec -i -w "/var/dump" webdock_php mysql -hmysql -uroot -proot -e "CREATE DATABASE gso"`

- Um einen __Dump einzuspielen__, kopieren Sie die SQL-Datei in das Dump-Verzeichnis. Falls der Dump eine Zip-Datei ist (mit der Endung .gz oder .zip), packen Sie diese aus. Benennen Sie die Datei dann zu "db_gso_restore.sql" um. Diesen Dump spielen Sie in die Datenbank "gso" wie folgt ein:
    `docker exec -i -w "/var/dump" webdock_php sh -c "mysql -hmysql -uroot -proot gso < /var/dump/db_gso_restore.sql"`
    Das kann bei großen Datenbanken eine Weile dauern. 

- Über das Tool mysqldump lassen sich wiederum __Dumps zur Datensicherung__ anfertigen. Mit dem folgenden Befehl wird ein Dump der Datenbank "gso" angelegt. An den Dateinamen werden auch gleich das aktuelle Datum und die Uhrzeit angehängt, dafür sorgt die sogenannte variable expansion hinter dem $-Zeichen:
    `docker exec webdock_php sh -c "mysqldump gso -hmysql -uroot -proot -r /var/dump/db-gso-$(get-date -f yyyy-MM-ddTHH-mm-ss-ffff).sql"`

Zur Vereinfachung der __Datensicherung__ finden Sie im dump-Ordner ein PowerShell-Script, je nach Szenario verwenden Sie das Script im Unterordner hyperv bzw. virtualbox. Das Script können Sie mit Doppelklick auf die dazugehörige Batch-Datei "docker_mysql_gso_dump.bat" aufrufen. Der Dump wird im übergeordneten dump-Verzeichnis abgelegt.


## 5. Webanwendung einrichten
Nach dem Starten der Container liefert der Webserver unter der Adresse http://localhost die Dateien im Unterordner html aus (bzw. mit docker-machine unter der entsprechenden IP-Adresse). Vorläufig liegt dort nur eine Datei index.php. Sie können in diesem Ordner oder auch in Unterverzeichnissen andere Webanwendungen ablegen. Wenn Sie eine Anwendung zum Beispiel im  Ordner "html/gso" eine Webanwendung einrichten, können Sie diese im Browser unter der Adresse http://localhost/gso/ aufrufen.

Wie eine bestimmte Anwendung eingerichtet werden muss, ist von Fall zu Fall unterschiedlich. Befolgen Sie hierzu die Dokumentation der Anwendung.